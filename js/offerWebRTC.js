
console.log("execute script 2");

var localConnection;
var xmlhttp
var candidate
var onicecount = 0
//var remoteConnection;

window.addEventListener("load",function(event){
	startup();
	console.log("All resources finished loading!");
});
window.addEventListener("unload",()=>{
  disconnectPeers();      // if not close , browser will remember the current status of button
});
function startup() {
  connectButton = document.getElementById('connectButton');
  disconnectButton = document.getElementById('disconnectButton');
  sendButton = document.getElementById('sendButton');
  messageInputBox = document.getElementById('message');
  receiveBox = document.getElementById('receivebox');

  // Set event listeners for user interface widgets

  connectButton.addEventListener('click', connectPeers, false);
  disconnectButton.addEventListener('click', disconnectPeers, false);
  sendButton.addEventListener('click', sendMessage, false);

  xmlhttp = new XMLHttpRequest()
  xmlhttp.onreadystatechange=() => {
    if(xmlhttp.readyState==4 && xmlhttp.status == 200){
        console.log("receive from server")
        console.log(xmlhttp.responseText)
        var r = parseAjax(xmlhttp.responseText)
        r.a.sdpMLineIndex = 0
        r.a.sdpMid = 'data'
        localConnection.addIceCandidate(r.a).catch(handleAddCandidateError);
        localConnection.setRemoteDescription(r.b).catch(handleCreateDescriptionError)
    }
  }

}

function parseAjax(a){

  var tr = JSON.parse(a)
  var r = {a:null, b:null}
  r.a = tr.a
  r.b = tr.b
  console.log("receive description")
  console.log(tr)
  console.log("receive candidate\n")
  console.log(r.a)
  return r
}
function unparseAjax(a,b){
  var c = {a:null, b: null}
  c.a = a
  c.b = b
  return JSON.stringify(c)
}

function connectPeers(){  

  localConnection = new RTCPeerConnection();

//remoteConnection = new RTCPeerConnection();  

  sendChannel = localConnection.createDataChannel("sendChannel");
  sendChannel.onopen = handleSendChannelStatusChange;
  sendChannel.onclose = handleSendChannelStatusChange;
  sendChannel.onmessage = echoOnmessage;

//  remoteConnection.ondatachannel = receiveChannelCallback;  

  // localConnection.onicecandidate = e => !e.candidate   // simple understand : function name -> input (parameter) -> output(return value)
          // .catch(handleAddCandidateError);             // ice is optional function ?
  
  localConnection.onicecandidate = e => {  

        if(e.candidate){
          if(onicecount == 0)
          {  
            candidate = new RTCIceCandidate(e.candidate)
            console.log(candidate.toJSON())
            // console.log(JSON.stringify(candidate.toJSON()))
            // console.log(JSON.parse(JSON.stringify(candidate.toJSON())))
            //var testcandidate1 = new RTCIceCandidate(JSON.parse(JSON.stringify(candidate.toJSON())))
            // var testcandidate2 = new RTCIceCandidate(candidate.toJSON)
            // console.log(testcandidate1)
            // console.log(testcandidate2)
            // why  candidate.toJSON() != JSON.parse(JSON.stringify(candidate.toJSON())) ?
  
            console.log("send to server candidate:")
            // console.log(e.candidate.toJSON())
            // console.log(localConnection.localDescription)
            xmlhttp.open("POST","/candi_desc",true)
            xmlhttp.setRequestHeader("Content-Type","application/json")
          // xmlhttp.setRequestHeader("Content-Length",)
            var c = unparseAjax(e.candidate,localConnection.localDescription)
            console.log(c)
            var testcandidate3 = new RTCIceCandidate(JSON.parse(c).a)
            // console.log(testcandidate3)
            xmlhttp.send(c)
  
            onicecount = 1
          }
        }
      
        console.log("local ice");
        if(!e.candidate) {console.log("e.candidate is null")}
        else {
            // localConnection.addIceCandidate(e.candidate).catch(handleAddCandidateError1)
        }
        return !e.candidate
     }


  localConnection.createOffer()
    .then(offer => localConnection.setLocalDescription(offer))
    .then(() =>    { 

      
    })

 //curl -i -H "Content-Type: application/json" -X POST ":"1","id":"b"}' http://localhost:5000/candi_desc
}

 function AjaxGet(remoteDescription){
    localConnection.setRemoteDescription(remoteDescription)
 }

function handleCreateDescriptionError(){
    console.log("set remotedescripton error")
}

function handleAddCandidateError(){
    console.log("add candidate error")
}

function handleAddCandidateError1(){
  console.log("add candidate error of 1")
}

function handleLocalAddCandidateSuccess() {
    connectButton.disabled = true;
  }


function handleSendChannelStatusChange(event) {
    if (sendChannel) {
      var state = sendChannel.readyState;
      console.log("connect status is: ")

      if (state === "open") {
        messageInputBox.disabled = false;
        messageInputBox.focus();
        sendButton.disabled = false;
        disconnectButton.disabled = false;
        connectButton.disabled = true;
      } else {

        console.log("other sendchannel status changed")
        messageInputBox.disabled = true;
        sendButton.disabled = true;
        connectButton.disabled = false;
        disconnectButton.disabled = true;
      }
    }
  }


function sendMessage() {
    var message = messageInputBox.value;
    sendChannel.send(message);
    
    // messageInputBox.value = "";
    // messageInputBox.focus();
  }

function echoOnmessage(event) {
    var el = document.createElement("p");
    var txtNode = document.createTextNode(event.data);

    el.appendChild(txtNode);
    receiveBox.appendChild(el);
}

function disconnectPeers() {

    // Close the RTCDataChannels if they're open.

    sendChannel.close();
    // receiveChannel.close();

    // Close the RTCPeerConnections

    localConnection.close();
    // remoteConnection.close();

    sendChannel = null;
    // receiveChannel = null;
    localConnection = null;
    // remoteConnection = null;

    // // Update user interface elements

    // connectButton.disabled = false;
    // disconnectButton.disabled = true;
    // sendButton.disabled = true;
}

