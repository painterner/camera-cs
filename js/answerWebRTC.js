console.log("execute script 2c");
var remoteConnection;

window.addEventListener("load",function(event){
        startup();
        console.log("All resources finished loading!");
});
window.addEventListener("unload",()=>{
  disconnectPeers();      // if not close , browser will remember the current status of button
});

function startup(){
	remoteConnection = new RTCPeerConnection();

	remoteConnection.ondatachannel = receiveChannelCallback;

        remoteConnection.onicecandidate = e => !e.candidate 
          .catch(handleAddCandidateError);

  	remoteConnection.createAnswer()
    	.then(answer => remoteConnection.setLocalDescription(answer))
    	.then(() => localConnection.setRemoteDescription(remoteConnection.localDescription))
    	.catch(handleCreateDescriptionError);

}

  function handleRemoteAddCandidateSuccess() {}


  function receiveChannelCallback(event) {
    receiveChannel = event.channel;
    receiveChannel.onmessage = handleReceiveMessage;
    receiveChannel.onopen = handleReceiveChannelStatusChange;
    receiveChannel.onclose = handleReceiveChannelStatusChange;
  }


 function handleReceiveChannelStatusChange(event) {
    if (receiveChannel) {
      console.log("Receive channel's status has changed to " +
                  receiveChannel.readyState);
    }
  }



  function handleReceiveMessage(event) {
    var el = document.createElement("p");
    var txtNode = document.createTextNode(event.data);

    el.appendChild(txtNode);
    receiveBox.appendChild(el);
  }
