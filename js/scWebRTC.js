console.log("execute script 2");

var localConnection;
var remoteConnection;

window.addEventListener("load",function(event){
	startup();
	console.log("All resources finished loading!");
});
window.addEventListener("unload",()=>{
  disconnectPeers();      // if not close , browser will remember the current status of button
});
function startup() {
  connectButton = document.getElementById('connectButton');
  disconnectButton = document.getElementById('disconnectButton');
  sendButton = document.getElementById('sendButton');
  messageInputBox = document.getElementById('message');
  receiveBox = document.getElementById('receivebox');

  // Set event listeners for user interface widgets

  connectButton.addEventListener('click', connectPeers, false);
  disconnectButton.addEventListener('click', disconnectPeers, false);
  sendButton.addEventListener('click', sendMessage, false);
}

function connectPeers(){  

  localConnection = new RTCPeerConnection();

  remoteConnection = new RTCPeerConnection();  

  sendChannel = localConnection.createDataChannel("sendChannel");
  sendChannel.onopen = handleSendChannelStatusChange;
  sendChannel.onclose = handleSendChannelStatusChange;

  remoteConnection.ondatachannel = receiveChannelCallback;  

  localConnection.onicecandidate = e => {  console.log("local ice");
        console.log(e.candidate)
        return  !e.candidate || remoteConnection.addIceCandidate(e.candidate)
          .catch(handleAddCandidateError) }
  
      remoteConnection.onicecandidate = e =>{
        console.log("server ice");
      return  !e.candidate
          || localConnection.addIceCandidate(e.candidate)
          .catch(handleAddCandidateError);
        }

  localConnection.createOffer()
     .then(offer => {console.log(offer); return localConnection.setLocalDescription(offer)}) //will call onicecandidate
     .then(() => remoteConnection.setRemoteDescription(localConnection.localDescription))    //will not call onicecandidate
    .then(() => remoteConnection.createAnswer())
    .then(answer => remoteConnection.setLocalDescription(answer))
    .then(() => localConnection.setRemoteDescription(remoteConnection.localDescription))
    .catch(handleCreateDescriptionError);
  
}

function handleCreateDescriptionError(){}

function handleAddCandidateError(){}


function handleLocalAddCandidateSuccess() {
    connectButton.disabled = true;
    console.log("add candidateSuccess")
  }

  function handleRemoteAddCandidateSuccess() {
    disconnectButton.disabled = false;
  }


function receiveChannelCallback(event) {
    receiveChannel = event.channel;
    receiveChannel.onmessage = handleReceiveMessage;
    receiveChannel.onopen = handleReceiveChannelStatusChange;
    receiveChannel.onclose = handleReceiveChannelStatusChange;
  }

function handleSendChannelStatusChange(event) {
    if (sendChannel) {
      var state = sendChannel.readyState;
      console.log("connect status is: ")

      if (state === "open") {
        messageInputBox.disabled = false;
        messageInputBox.focus();
        sendButton.disabled = false;
        disconnectButton.disabled = false;
        connectButton.disabled = true;
      } else {
        messageInputBox.disabled = true;
        sendButton.disabled = true;
        connectButton.disabled = false;
        disconnectButton.disabled = true;
      }
    }
  }


function handleReceiveChannelStatusChange(event) {
    if (receiveChannel) {
      console.log("Receive channel's status has changed to " +
                  receiveChannel.readyState);
    }
  }


function sendMessage() {
    var message = messageInputBox.value;
    sendChannel.send(message);
    
    messageInputBox.value = "";
    messageInputBox.focus();
  }


function handleReceiveMessage(event) {
    var el = document.createElement("p");
    var txtNode = document.createTextNode(event.data);

    el.appendChild(txtNode);
    receiveBox.appendChild(el);
  }


function disconnectPeers() {

    // Close the RTCDataChannels if they're open.

    sendChannel.close();
    receiveChannel.close();

    // Close the RTCPeerConnections

    localConnection.close();
    remoteConnection.close();

    sendChannel = null;
    receiveChannel = null;
    localConnection = null;
    remoteConnection = null;

    // Update user interface elements

    connectButton.disabled = false;
    disconnectButton.disabled = true;
    sendButton.disabled = true;
}

