#!/usr/bin/python3

import sys,os
import asyncio
from rtcThread import rtcThread as rtcThread
from flask import Flask as flask, render_template, send_from_directory,url_for,request



app = flask(__name__,static_folder='',static_url_path='')

@app.route('/')
def home():
    return render_template('home.html') 
  #  return render_template('webRTC.html')

@app.route('/localrtc')
def webRTC():
    return render_template('localwebRTC.html')

@app.route('/rtc')
def scWebRTC():
    return render_template('scWebRTC.html')

@app.route('/candi_desc',methods=["POST"])
def candi_desc():
    if(request.method == 'POST'):
        thread = rtcThread(request.data)
        thread.start()

    print('send to client:' + thread.get() )            
    return thread.get()


@app.route('/00.php')
def php():
    print('get 00.php')
    return "i am  00.php"
    #return subprocess.call(['php',url_for('static',filename='static/00.php')])

if __name__ == "__main__":
    app.run()
    
