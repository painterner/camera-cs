#!/usr/bin/env python
# -*- coding:utf-8 -*-
import sys,os
import socket
import time
import threading

# note: must use python3

class htbt(threading.Thread):
    def __init__(self,HOST,PORT):
        threading.Thread.__init__(self)
        self.maxHtbtLen = 10 
        self.HOST = HOST
        self.PORT = PORT
        self.sockOpenFlag = False
        
    def run(self):
        tcount = 0
        while True:
            with socket.socket(type=socket.SOCK_STREAM) as s:
                s.connect((self.HOST,self.PORT))
                while True:
                    tcount = tcount + 1
                    time.sleep(1)
                    s.sendall(b'htbt')
                    data = s.recv(self.maxHtbtLen)
                    print(data)

                    if(data == b"open"):
                        self.sockOpenFlag = True
                    elif(data == b"close"):
                        self.sockOpenFlag = False


class client():
    def __init__(self):
        # self.HOST = '127.0.0.1'; self.HtHost = '127.0.0.1'
        # baiduyun vps
        self.HOST = '180.76.147.23'; self.HtHost = '180.76.147.23'
        self.PORT = 50028 ; self.HtPort = 50029
        self.frameCount = 0
        self.dataLen = 320*240

        self.ht = htbt(self.HtHost,self.HtPort)
        self.ht.start()
        
    def client(self):
        while True:
            if(self.ht.sockOpenFlag == False):
                continue
            with socket.socket(type=socket.SOCK_STREAM) as s:
                s.connect((self.HOST,self.PORT))
                while True:
                    if(self.ht.sockOpenFlag == False):
                        break
                    time.sleep(1)
                    try:
                        s.sendall(b'hello word')
                    except:
                        break
                    print("oo")
                    data = s.recv(self.dataLen)
                    print(repr(data))
                print("disconnect")
        # client()

if __name__ == "__main__":
    client = client()
    client.client()

