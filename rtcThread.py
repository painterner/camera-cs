import argparse
import asyncio
import json
import re
import threading
import time
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.rtcpeerconnection import get_default_candidate

class rtcThread(threading.Thread):
    def __init__(self,request_data):
        threading.Thread.__init__(self)
        self.__rdata = request_data
        self.__getflag = False
        # print("\nrequest data:reading.Thread ")
        # print(request.data)

    def get(self):
        while(self.__getflag == False):
            time.sleep(0.2)
        return self.__event.data
        
    def run(self):
        asyncio.set_event_loop(asyncio.new_event_loop())
        self.__event = asyncio.Event()
        
        async def create_pc():
            pc = RTCPeerConnection()
            return pc
        async def run_answer():

            pc = await create_pc()
            def channel_log(channel,t,message):
                print('channel(%s) %s %s' % (channel.label,t,message))

            @pc.on('datachannel')
            def on_datachannel(channel):
                @channel.on('message')
                def on_message(message):
                    channel_log(channel, '>', message)
                    channel.send('echo: '+message)

            offer = json.loads(self.__rdata)
            # print('\n object request data')
            # print(offer)

            # pc.addIceCandidate(offer['a'])    # need await ?

            rdesc_dict=offer["b"]
            #print(type(rdesc_dict['sdp']))
            rsessiondesc=RTCSessionDescription(sdp=rdesc_dict['sdp'],type=rdesc_dict['type'])
            await pc.setRemoteDescription(rsessiondesc)

            await pc.setLocalDescription(await pc.createAnswer())
            # print("\nlocalDescription: ")
            # print(pc.localDescription)
            candidate = pc.sctp.transport.transport
            candidate = get_default_candidate(candidate)
            print(candidate)
            revalue = pc.localDescription.sdp
            r = re.search(r'a=candidate(.*)\\r\\na=end-of-candidate',json.dumps(revalue))
            r = 'candidate'+r.group(1)
            r = r.split('\\r\\n')[0]
            print('\n first condidate')
            print(r)

            c = {}
            c['a'] = {"candidate":r,"sdpMLineIndex":candidate.sdpMLineIndex,"sdpMid":candidate.sdpMid}
            c['b'] = {'sdp':pc.localDescription.sdp,'type':pc.localDescription.type}
            jstr = json.dumps(c)
            self.__event.data = jstr
            self.__getflag = True
            # print(jstr)
            await self.__event.wait()
            return jstr
            
        coro = run_answer()
         # run event loop
        loop = asyncio.get_event_loop()
        try:
            jstr=loop.run_until_complete(coro)
            # print(jstr)
            jstr=format(jstr)
            # print(jstr)
        except KeyboardInterrupt:
            pass