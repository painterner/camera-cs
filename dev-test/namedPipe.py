#-*- coding:utf-8 -*-

import sys,os
import time

read_path= "./pipe.f"
write_path="./pipe.f"

try:
    os.mkfifo(write_path)
except OSError as e:
    print("mkfifo error:",e)

wf = os.open(write_path,os.O_SYNC | os.O_CREAT | os.O_WRONLY)

print("wf type:",type(wf))

#time.sleep(1)

os.write(wf,b"i am sender1")
time.sleep(0.1)
os.write(wf,b"i am sender2")
time.sleep(0.1)
os.write(wf,b"i am sender3")

os.close(wf)
