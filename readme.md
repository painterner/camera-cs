关于设置zero网络通过3b连接inet，并且希望3b或者zero可以动态获取dncp地址的研究。
经过长达1天的考虑。
记录 2018 - 7 - 20
1. 设想电脑配置成dhcp server，然后zero通过usb来自动设置自己的网络。
   失败，可能没找到合适的教程。

2. 设想zero配置成dhcp server。
   结果失败。

3. 发现有用信息
   由于每次上电后电脑检测的zero网络适配器的mac地址都会改变，相当于一个新的适配器，所以每次都要配置网络地址。
   以往用ui配置时会写入网关，但是若这样做，会导致系统自动增加一个默认网关并且置为首要默认网关（掩盖了可以上网的网关），但是此网关无法连接外网（上网流程变为，查找本地规则，本地规则一般都会映射到默认网关，然后默认网关找到的确是usb网关，于是无法上网）。
   所以现在配置只需要把网关那项不填即可。
   zero /etc/netowrk/interface  中关于甚至static usb0含有gateway一项，如果comment的话，会导致系统无法自动添加默认路由。

综上，现在的work around是每次连接时主机重新配置网络，但是不配置网关，然后开启转发功能:
* 1 /etc/sysctl.conf  -> net.ipv4.ip_forward=1  
* 2 iptables -t nat -A POSTROUTING -o wlan0 -j MASQUERADE  
* 3 iptables -A FORWARD -i wlan0 -o usb0 -m state --state RELATED,ESTABLISHED -j ACCEPT  
* 4 iptables -A FORWARD -i usb0 -o wlan0 -j ACCEPT  
* 5 iptables-save > /etc/iptables.ipv4.nat  
然后启动脚本中添加:  
	iptables-restore < /etc/iptables.ipv4.nat  

被转发的pc可能需要配置dns解析, 在 /etc/resolv.conf中添加
* nameserver 114.114.114.114  ## 国内
* nameserver 8.8.8.8  ## google
* nameserver 8.8.4.4

后续需要做的:
   配置主机或者zero可以为dncp server。

raspivid -w 1920 -h 1080 -fps 30 -o tcp://192.168.7.1:50007 -t 0
# 1920*1080 connect to tcp://xxx  -t 0 infinitly send data , format h264
# how to send raw data?
# ref https://www.raspberrypi.org/documentation/raspbian/applications/camera.md


about flask:
   path find: system auto find the "static" target as root path, default static=static , if you want to use workspace as root path, you can use:
	Flask(__name__,static_folder='',static_url_path='')
then use
	url_for('static', filename='js/xxx') (js is subfolder of workspace)


#history:
__2018.8.3__
success use webrtc in local host loopback network,though have some waring such as end of js has "candidate add error" and "multi candidate problem" find the problem is that coroutine not execute event.wait() for forever loop
